package databases;

import entities.Producer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

public
final
class ProducerDatabase implements Database<Producer> {
    private final
    ArrayList<Producer> producers = new ArrayList<>();

    @Override
    public Collection<Producer> getCollection() {
        return producers;
    }

    @Override
    public Producer getById(Integer id) {
        return producers.stream().filter(producer -> producer.getId().equals(id)).findFirst()
                .orElse(null);
    }

    @Override
    public Stream<Producer> getStream() {
        return producers.stream();
    }

    @Override
    public void add(Producer val) {
        producers.add(val);
    }
}
