package databases;

import java.util.Collection;
import java.util.stream.Stream;

public
interface Database<T> {
    /**
     * Get underlying collection
     *
     * @return collection
     */
    Collection<T> getCollection();

    /**
     * Get value by id, if possible
     *
     * @param id id of the required entity
     * @return entity if it has an id
     */
    T getById(Integer id);

    /**
     * Get underlying stream
     *
     * @return stream
     */
    Stream<T> getStream();

    /**
     * Adds value to collection
     *
     * @param val value
     */
    void add(T val);
}
