package databases;

import entities.Consumer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

public
final
class ConsumerDatabase implements Database<Consumer> {

    private final ArrayList<Consumer> consumers = new ArrayList<>();

    @Override
    public Collection<Consumer> getCollection() {
        return consumers;
    }

    @Override
    public Consumer getById(final Integer id) {
        return consumers.stream().filter(consumer -> consumer.getId().equals(id)).findFirst()
                .orElse(null);
    }

    @Override
    public Stream<Consumer> getStream() {
        return consumers.stream();
    }

    @Override
    public void add(final Consumer val) {
        consumers.add(val);
    }

    public ArrayList<Consumer> getConsumers() {
        return consumers;
    }
}
