package databases;

import entities.Contract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

public
final
class ContractDatabase implements Database<Contract> {
    private final ArrayList<Contract> contracs = new ArrayList<>();

    @Override
    public Collection<Contract> getCollection() {
        return contracs;
    }

    @Override
    public Contract getById(final Integer id) {
        return null;
    }

    @Override
    public Stream<Contract> getStream() {
        return contracs.stream();
    }

    @Override
    public void add(final Contract val) {
        contracs.add(val);
    }

    public ArrayList<Contract> getContracs() {
        return contracs;
    }
}
