package databases;

import entities.Distributor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

public
final
class DistributorDatabase implements Database<Distributor> {
    private final ArrayList<Distributor> distributors = new ArrayList<>();

    @Override
    public Collection<Distributor> getCollection() {
        return distributors;
    }

    @Override
    public Distributor getById(final Integer id) {
        return distributors.stream().filter(distributor -> distributor.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    public Stream<Distributor> getStream() {
        return distributors.stream();
    }

    @Override
    public void add(final Distributor val) {
        distributors.add(val);
    }

    public ArrayList<Distributor> getDistributors() {
        return distributors;
    }
}
