import com.fasterxml.jackson.databind.ObjectMapper;
import outputdata.OutputObject;
import simulation.SimulationManager;
import testinputdata.InputData;

import java.io.File;
import java.io.IOException;

public final
class Main {

    private Main() {

    }

    /**
     * Main simulation function
     *
     * @param args string array with input and output file names
     * @throws IOException if fails to open files
     */
    public static void main(final String[] args) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final InputData data = mapper.readValue(new File(args[0]), InputData.class);

        final var manager = new SimulationManager();
        manager.runSimulation(data);
        final OutputObject out =
                new OutputObject(manager.getConsumers(), manager.getDistributors(),
                        manager.getProducers());
        mapper.writeValue(new File(args[1]), out);
    }
}
