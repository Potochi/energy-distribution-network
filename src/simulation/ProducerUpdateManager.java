package simulation;

import databases.Database;
import entities.Producer;
import testinputdata.MonthlyUpdateData;

import java.util.List;

public final
class ProducerUpdateManager extends SimulationEntity {

    private final Database<Producer> producers;
    private final List<MonthlyUpdateData> changes;
    private Integer currentTick = 0;

    public ProducerUpdateManager(List<MonthlyUpdateData> changes, Database<Producer> producers) {
        this.changes = changes;
        this.producers = producers;
    }

    @Override
    protected void tickInternal() {
        var currentChanges = changes.get(currentTick);
        for (var change : currentChanges.getProducerChangeData()) {
            producers.getById(change.getId()).update(change.getEnergyPerDistributor());
        }
        currentTick++;
    }
}
