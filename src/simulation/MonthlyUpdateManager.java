package simulation;

import databases.Database;
import entities.Consumer;
import entities.Distributor;
import factories.ConsumerFactory;
import testinputdata.MonthlyUpdateData;

import java.util.List;

public
final
class MonthlyUpdateManager extends SimulationEntity {

    private final Database<Consumer> consumers;
    private final Database<Distributor> distributors;

    private final List<MonthlyUpdateData> updates;
    private Integer currentMonth = 0;

    public MonthlyUpdateManager(
            final Database<Consumer> consumers1,
            final Database<Distributor> distributors1, final List<MonthlyUpdateData> updates) {
        this.consumers = consumers1;
        this.distributors = distributors1;
        this.updates = updates;
    }

    /**
     * Updates distributors and adds consumers to the simulation according
     * to the update data for the current turn
     */
    @Override
    protected void tickInternal() {
        final var costChanges = updates.get(currentMonth).getCostChanges();
        for (final var change : costChanges) {
            distributors.getById(change.getId()).updateCosts(change);
        }
        final var newConsumers = updates.get(currentMonth).getNewConsumers();
        for (final var consumer : newConsumers) {
            consumers.add(ConsumerFactory.getInstance().create(consumer));
        }
        currentMonth++;
    }
}
