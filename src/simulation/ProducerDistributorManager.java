package simulation;

import databases.Database;
import entities.Distributor;

public final
class ProducerDistributorManager extends SimulationEntity {
    private final Database<Distributor> distributors;

    public ProducerDistributorManager(Database<Distributor> distributors) {
        this.distributors = distributors;
    }

    @Override
    protected void tickInternal() {
        distributors.getStream().filter(Distributor::getCheckProducers).forEach(
                Distributor::updateProducers);
    }
}
