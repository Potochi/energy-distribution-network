package simulation;

import databases.Database;
import entities.Distributor;

public
final
class DistributorPriceUpdater extends SimulationEntity {
    private final Database<Distributor> distributors;

    public DistributorPriceUpdater(final Database<Distributor> distributors) {
        this.distributors = distributors;
    }

    /**
     * Updates all the distributors prices
     */
    @Override
    protected void tickInternal() {
        distributors.getStream().forEach(Distributor::updateAndRemove);
    }
}
