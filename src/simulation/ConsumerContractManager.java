package simulation;

import databases.ContractDatabase;
import databases.Database;
import entities.Consumer;
import entities.Contract;
import entities.Distributor;

import java.util.Comparator;

public
final
class ConsumerContractManager extends SimulationEntity {

    private final Database<Consumer> consumers;
    private final Database<Distributor> distributors;
    private final Database<Contract> contracts = new ContractDatabase();


    public ConsumerContractManager(final Database<Consumer> consumers,
                                   final Database<Distributor> distributors) {
        this.consumers = consumers;
        this.distributors = distributors;
    }

    private Contract createContract(final Distributor d, final Consumer c) {
        final var contract = new Contract(d, c);
        c.setHasContract(true);
        d.addContract(contract);
        return contract;
    }

    private Distributor getLowestContractor() {
        return distributors.getStream().filter(Distributor::isActive)
                .min(Comparator.comparingInt(Distributor::getDistributorPrice))
                .orElse(null);
    }

    /**
     * Removes inactive contracts, creates contracts for consumers that don't have one
     * and runs one tick for all contracts
     */
    @Override
    protected void tickInternal() {
        final Distributor lowestCost = getLowestContractor();
        if (lowestCost == null) {
            return;
        }
        contracts.getCollection().removeIf(contract -> !contract.isActive());
        consumers.getStream().filter(Consumer::isActive)
                .filter(consumer -> !consumer.isHasContract())
                .forEach(consumer -> contracts.add(createContract(lowestCost, consumer)));
        contracts.getStream().filter(Contract::isActive).forEach(Contract::tick);
    }
}
