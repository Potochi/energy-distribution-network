package simulation;

import databases.Database;
import entities.Consumer;

public
final
class ConsumerPaymentManager extends SimulationEntity {

    private final Database<Consumer> consumers;

    public ConsumerPaymentManager(final Database<Consumer> consumers) {
        this.consumers = consumers;
    }

    /**
     * Runs the consumer's tick function, adding the income to their budget
     */
    @Override
    protected void tickInternal() {
        consumers.getStream().filter(Consumer::isActive).forEach(Consumer::tick);
    }
}
