package simulation;

import databases.ConsumerDatabase;
import databases.Database;
import databases.DistributorDatabase;
import databases.ProducerDatabase;
import entities.Consumer;
import entities.Distributor;
import entities.Producer;
import factories.ConsumerFactory;
import factories.DistributorFactory;
import factories.ManagerFactory;
import testinputdata.InputData;

import java.util.ArrayList;
import java.util.Arrays;

public
final
class SimulationManager extends SimulationEntity {

    private final Database<Consumer> consumers = new ConsumerDatabase();
    private final Database<Distributor> distributors = new DistributorDatabase();
    private final Database<Producer> producers = new ProducerDatabase();
    private final ArrayList<SimulationEntity> entities = new ArrayList<>();

    public SimulationManager() {

    }

    public Database<Producer> getProducers() {
        return producers;
    }

    public Database<Consumer> getConsumers() {
        return consumers;
    }

    public Database<Distributor> getDistributors() {
        return distributors;
    }

    /**
     * Main simulation loop
     *
     * @param simulationData initial simulation data
     */
    public void runSimulation(final InputData simulationData) {
        final int numberOfTurns = simulationData.getTurns();

        for (final var data : simulationData.getData().getConsumers()) {
            consumers.add(ConsumerFactory.getInstance().create(data));
        }

        for (final var data : simulationData.getData().getDistributors()) {
            distributors.add(DistributorFactory.getInstance().create(data, producers));
        }

        for (final var data : simulationData.getData().getProducers()) {
            producers.add(new Producer(data.getEnergyType(), data.getPriceKW(),
                    data.getMaxDistributors(), data.getId(),
                    data.getEnergyPerDistributor()));
        }

        var updates = Arrays.asList(simulationData
                .getMonthlyUpdates());

        final var updateManager =
                ManagerFactory.getINSTANCE()
                        .getUpdateManager(consumers, distributors, updates);

        final var priceUpdater = ManagerFactory.getINSTANCE()
                .getPriceUpdater(distributors);

        final var consumerIncome = ManagerFactory.getINSTANCE()
                .getPaymentManager(consumers);

        final var contractManager =
                ManagerFactory.getINSTANCE().getContractManager(consumers, distributors);


        entities.add(updateManager);
        entities.add(priceUpdater);
        entities.add(consumerIncome);
        entities.add(contractManager);

        final var producerManager =
                new ProducerUpdateManager(Arrays.asList(simulationData.getMonthlyUpdates()),
                        producers);

        final var distripod = new ProducerDistributorManager(distributors);


        distripod.tick();
        priceUpdater.tick();
        consumerIncome.tick();
        contractManager.tick();
        distributors.getStream().filter(Distributor::isActive).forEach(Distributor::tick);

        for (int i = 0; i < numberOfTurns; i++) {
            tick();
            distributors.getStream().filter(Distributor::isActive).forEach(Distributor::tick);
            producerManager.tick();
            distripod.tick();
            producers.getStream().forEach(Producer::tick);
        }
    }

    @Override
    protected void tickInternal() {
        entities.forEach(SimulationEntity::tick);
    }

    public ArrayList<SimulationEntity> getEntities() {
        return entities;
    }
}
