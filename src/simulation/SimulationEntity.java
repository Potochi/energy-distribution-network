package simulation;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract
class SimulationEntity {
    @JsonIgnore
    private SimulationEntityState state = SimulationEntityState.ACTIVE;

    protected abstract void tickInternal();

    /**
     * The entity's public tick funcion
     * Calls tickInternal if the entity is active
     */
    public final void tick() {
        if (state == SimulationEntityState.ACTIVE) {
            tickInternal();
        }
    }

    /**
     * Activate entity
     */
    public final void activate() {
        state = SimulationEntityState.ACTIVE;
    }

    /**
     * Deactivate entity
     */
    public final void deactivate() {
        state = SimulationEntityState.INACTIVE;
    }

    public final boolean isActive() {
        return state == SimulationEntityState.ACTIVE;
    }

}
