package strategies.comparators;

import entities.Producer;

import java.util.Comparator;

public final
class ProducerComparators {

    public static final
    Comparator<Producer> PRICE_QUANTITY_COMPARATOR = (o1, o2) -> {
        int result = o1.getPrice().compareTo(o2.getPrice());
        if (result == 0) {
            result = -o1.getDistribution().compareTo(o2.getDistribution());
            if (result == 0) {
                result = o1.getId().compareTo(o2.getId());
            }
        }
        return result;
    };

    public static final
    Comparator<Producer> QUANTITY_COMPARATOR = (o1, o2) -> {
        int result = -o1.getDistribution().compareTo(o2.getDistribution());
        if (result == 0) {
            result = o1.getId().compareTo(o2.getId());
        }
        return result;
    };

    public static final
    Comparator<Producer> GREEN_COMPARATOR = (o1, o2) -> {
        int result;
        if (o1.getEnergyType().isRenewable() && !o2.getEnergyType().isRenewable()) {
            result = -1;
        } else if (o2.getEnergyType().isRenewable() && !o1.getEnergyType().isRenewable()) {
            result = 1;
        } else {
            result = o1.getPrice().compareTo(o2.getPrice());
            if (result == 0) {
                result = -o1.getDistribution().compareTo(o2.getDistribution());
                if (result == 0) {
                    result = o1.getId().compareTo(o2.getId());
                }
            }
        }
        return result;
    };

    private ProducerComparators() {

    }
}
