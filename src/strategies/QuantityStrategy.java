package strategies;

import databases.Database;
import entities.Producer;
import strategies.comparators.ProducerComparators;

import java.util.Iterator;

public
final
class QuantityStrategy extends ProducerStrategy {
    public QuantityStrategy(Database<Producer> producers) {
        super(producers);
    }

    @Override
    public Iterator<Producer> getIterator() {
        return producers.getStream().sorted(ProducerComparators.QUANTITY_COMPARATOR).iterator();
    }

    @Override
    public String toString() {
        return "QUANTITY";
    }
}
