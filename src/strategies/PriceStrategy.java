package strategies;

import databases.Database;
import entities.Producer;
import strategies.comparators.ProducerComparators;

import java.util.Iterator;

public
final
class PriceStrategy extends ProducerStrategy {
    public PriceStrategy(Database<Producer> producers) {
        super(producers);
    }

    @Override
    public Iterator<Producer> getIterator() {
        return producers.getStream().sorted(ProducerComparators.PRICE_QUANTITY_COMPARATOR)
                .iterator();
    }

    @Override
    public String toString() {
        return "PRICE";
    }
}
