package strategies;

import databases.Database;
import entities.Producer;

import java.util.Iterator;

import static strategies.comparators.ProducerComparators.GREEN_COMPARATOR;

public
final
class GreenStrategy extends ProducerStrategy {

    public GreenStrategy(Database<Producer> producers) {
        super(producers);
    }


    @Override
    public Iterator<Producer> getIterator() {
        return producers.getStream()
                .sorted(GREEN_COMPARATOR)
                .iterator();
    }

    @Override
    public String toString() {
        return "GREEN";
    }
}
