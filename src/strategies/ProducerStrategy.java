package strategies;

import databases.Database;
import entities.Producer;

import java.util.Iterator;

public
abstract
class ProducerStrategy {
    protected final
    Database<Producer> producers;

    public ProducerStrategy(Database<Producer> producers) {
        this.producers = producers;
    }

    /**
     * Get a producer using the strategy
     *
     * @return producer
     */
    public abstract Iterator<Producer> getIterator();
}
