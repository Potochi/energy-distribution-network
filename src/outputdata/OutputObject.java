package outputdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import databases.Database;
import entities.Consumer;
import entities.Distributor;
import entities.Producer;

import java.util.ArrayList;

public
final
class OutputObject {
    @JsonIgnoreProperties({"bankrupt"})

    @JsonProperty("consumers")
    private final
    ArrayList<ConsumerOutputData> consumers = new ArrayList<>();
    @JsonProperty("distributors")
    private final
    ArrayList<DistributorOutputData> distributors = new ArrayList<>();
    @JsonProperty("energyProducers")
    private final ArrayList<ProducerOutputData> producers = new ArrayList<>();

    public OutputObject(final Database<Consumer> c, final Database<Distributor> d,
                        final Database<Producer> p) {
        c.getStream().forEach(consumer -> consumers.add(new ConsumerOutputData(consumer)));
        d.getStream()
                .forEach(distributor -> distributors.add(new DistributorOutputData(distributor)));
        p.getStream().forEach(producer -> producers.add(new ProducerOutputData(producer)));

    }

    public ArrayList<ConsumerOutputData> getConsumers() {
        return consumers;
    }

    public ArrayList<DistributorOutputData> getDistributors() {
        return distributors;
    }
}
