package outputdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.Distributor;

import java.util.ArrayList;

public
final
class DistributorOutputData {

    @JsonIgnoreProperties("bankrupt")

    /*
    "distributors" : [ {
    "id" : 0,
    "energyNeededKW" : 1956,
    "contractCost" : 16,
    "budget" : 146,
    "producerStrategy" : "GREEN",
    "isBankrupt" : false,
    "contracts" : [ {
      "consumerId" : 0,
      "price" : 16,
      "remainedContractMonths" : 5
    } ]
  } ],
     */

    @JsonProperty("id")
    private final Integer id;
    @JsonProperty("budget")
    private final Integer budget;
    @JsonProperty("isBankrupt")
    private final boolean bank;
    @JsonProperty("energyNeededKW")
    private final Integer energyNeededKW;
    @JsonProperty("producerStrategy")
    private final String producerStrategy;
    @JsonProperty("contractCost")
    private final Integer contractCost;
    @JsonProperty("contracts")
    private final
    ArrayList<ContractOutputData> contracts = new ArrayList<>();

    public DistributorOutputData(final Distributor d) {
        this.id = d.getId();
        this.budget = d.getBudget();
        this.bank = !d.isActive();
        this.energyNeededKW = d.getEnergyNeeded();
        this.producerStrategy = d.getStrategy().toString();
        this.contractCost = d.getContractCost();
        d.getContracts().getStream()
                .forEach(contract -> contracts.add(new ContractOutputData(contract)));
    }

    public Integer getContractCost() {
        return contractCost;
    }

    public Integer getEnergyNeededKW() {
        return energyNeededKW;
    }

    public String getProducerStrategy() {
        return producerStrategy;
    }

    public Integer getId() {
        return id;
    }

    public Integer getBudget() {
        return budget;
    }

    public boolean isBank() {
        return bank;
    }

    public ArrayList<ContractOutputData> getContracts() {
        return contracts;
    }
}
