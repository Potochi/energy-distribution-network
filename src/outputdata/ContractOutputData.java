package outputdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.Contract;

public
final
class ContractOutputData {

    @JsonIgnoreProperties({"bankrupt"})

    @JsonProperty("consumerId")
    private final Integer consumerId;
    @JsonProperty("price")
    private final Integer price;
    @JsonProperty("remainedContractMonths")
    private final Integer remainedContractMonths;

    public ContractOutputData(final Contract contract) {
        this.consumerId = contract.getConsumerId();
        this.price = contract.getPrice();
        this.remainedContractMonths = contract.getRemaining();
    }

    public Integer getConsumerId() {
        return consumerId;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getRemainedContractMonths() {
        return remainedContractMonths;
    }
}
