package outputdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.Consumer;

public
final
class ConsumerOutputData {

    @JsonIgnoreProperties({"bankrupt"})

    @JsonProperty("id")
    private final Integer id;
    @JsonProperty("isBankrupt")
    private final boolean bank;
    @JsonProperty("budget")
    private final Integer budget;

    public ConsumerOutputData(final Consumer c) {
        this.id = c.getId();
        this.bank = !c.isActive();
        this.budget = c.getBudget();
    }

    public Integer getId() {
        return id;
    }

    public boolean isBank() {
        return bank;
    }

    public Integer getBudget() {
        return budget;
    }

}
