package outputdata;


/*
}
  ],
  "energyProducers": [
    {
      "id": 0,
      "maxDistributors": 10,
      "priceKW": 0.01,
      "energyType": "WIND",
      "energyPerDistributor": 2695,
      "monthlyStats": [
        {
          "month": 1,
          "distributorsIds": [
            0
          ]
        },
        {
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import entities.Producer;

import java.util.ArrayList;

public final
class ProducerOutputData {
    @JsonProperty("id")
    private final Integer id;
    @JsonProperty("maxDistributors")
    private final Integer maxDistributors;
    @JsonProperty("priceKW")
    private final Double priceKW;
    @JsonProperty("energyType")
    private final String energyType;
    @JsonProperty("energyPerDistributor")
    private final Integer energyPerDistributor;
    @JsonProperty("monthlyStats")
    private final
    ArrayList<Stats> monthlyStats = new ArrayList<>();


    public ProducerOutputData(Producer p) {
        this.id = p.getId();
        this.maxDistributors = p.getMaxDistributor();
        this.priceKW = p.getPrice();
        this.energyType = p.getEnergyType().toString();
        this.energyPerDistributor = p.getDistribution();

        var ids = p.getDistributorIdLog();
        for (int i = 0; i < ids.size(); i++) {
            var s = new Stats(i, ids.get(i));
            monthlyStats.add(s);
        }
    }

    public Integer getId() {
        return id;
    }

    public Integer getMaxDistributors() {
        return maxDistributors;
    }

    public Double getPriceKW() {
        return priceKW;
    }

    public String getEnergyType() {
        return energyType;
    }

    public Integer getEnergyPerDistributor() {
        return energyPerDistributor;
    }

    public ArrayList<Stats> getMonthlyStats() {
        return monthlyStats;
    }

    static
    class Stats {
        @JsonProperty("month")
        private final Integer month;
        @JsonProperty("distributorsIds")
        private final
        ArrayList<Integer> distributorsIds = new ArrayList<>();

        Stats(Integer month, ArrayList<Integer> ids) {
            this.month = month + 1;
            ids.stream().sorted().forEach(distributorsIds::add);
        }

        public Integer getMonth() {
            return month;
        }

        public ArrayList<Integer> getDistributorsIds() {
            return distributorsIds;
        }
    }

}
