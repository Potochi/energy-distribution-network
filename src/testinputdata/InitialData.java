package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Arrays;

@JsonRootName("initialData")
public
final
class InitialData {
    private final ConsumerInputData[] consumers;
    private final DistributorInputData[] distributors;
    private final ProducerInputData[] producers;

    @JsonCreator
    public InitialData(
            @JsonProperty("consumers") ConsumerInputData[] consumers,
            @JsonProperty("distributors") DistributorInputData[] distributors,
            @JsonProperty("producers") ProducerInputData[] producers) {
        this.consumers = consumers;
        this.distributors = distributors;
        this.producers = producers;
    }

    @Override
    public String toString() {
        return "InitialData{" + "consumers=" + Arrays.toString(consumers)
                + ", distributors=" + Arrays.toString(distributors)
                + ", producers=" + Arrays.toString(producers)
                + '}';
    }

    public ConsumerInputData[] getConsumers() {
        return consumers;
    }

    public DistributorInputData[] getDistributors() {
        return distributors;
    }

    public ProducerInputData[] getProducers() {
        return producers;
    }
}
