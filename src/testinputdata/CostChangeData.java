package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public
final
class CostChangeData {
    private final int id;
    private final int infrastructureCost;

    @JsonCreator
    public CostChangeData(
            @JsonProperty("id") final
            int id,
            @JsonProperty("infrastructureCost") final
            int infrastructureCost) {
        this.id = id;
        this.infrastructureCost = infrastructureCost;
    }

    public int getId() {
        return id;
    }

    public int getInfrastructureCost() {
        return infrastructureCost;
    }

}
