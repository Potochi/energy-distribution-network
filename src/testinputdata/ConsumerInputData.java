package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public
class ConsumerInputData {
    private final int id;
    private final int initialBudget;
    private final int monthlyIncome;

    @JsonCreator
    public ConsumerInputData(
            @JsonProperty("id") final
            int id,
            @JsonProperty("initialBudget") final
            int initialBudget,
            @JsonProperty("monthlyIncome") final
            int monthlyIncome) {
        this.id = id;
        this.initialBudget = initialBudget;
        this.monthlyIncome = monthlyIncome;
    }

    @Override
    public final String toString() {
        return "ConsumerInputData{" + "id=" + id + ", initialBudget=" + initialBudget
                + ", monthlyIncome=" + monthlyIncome
                + '}';
    }

    public final int getId() {
        return id;
    }

    public final int getInitialBudget() {
        return initialBudget;
    }

    public final int getMonthlyIncome() {
        return monthlyIncome;
    }
}
