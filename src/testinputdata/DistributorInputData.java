package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public
final
class DistributorInputData {
    private final int id;
    private final int contractLength;
    private final int initialBudget;
    private final int infrastructureCost;
    private final int productionCost;
    private final int energyNeeded;
    private final String strategy;


    @JsonCreator
    public DistributorInputData(
            @JsonProperty("id") final
            int id,
            @JsonProperty("contractLength") final
            int contractLength,
            @JsonProperty("initialBudget") final
            int initialBudget,
            @JsonProperty("initialInfrastructureCost") final
            int infrastructureCost,
            @JsonProperty("initialProductionCost") final
            int productionCost,
            @JsonProperty("energyNeededKW") final
            int energyNeeded,
            @JsonProperty("producerStrategy") final
            String strategy
    ) {
        this.id = id;
        this.contractLength = contractLength;
        this.initialBudget = initialBudget;
        this.infrastructureCost = infrastructureCost;
        this.productionCost = productionCost;
        this.energyNeeded = energyNeeded;
        this.strategy = strategy;
    }


    public int getId() {
        return id;
    }

    public int getContractLength() {
        return contractLength;
    }

    public int getInitialBudget() {
        return initialBudget;
    }

    public int getInfrastructureCost() {
        return infrastructureCost;
    }

    public int getEnergyNeeded() {
        return energyNeeded;
    }

    public String getStrategy() {
        return strategy;
    }

    public int getProductionCost() {
        return productionCost;
    }
}
