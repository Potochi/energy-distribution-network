package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public
final
class InputData {
    private final int turns;
    private final InitialData data;

    private final MonthlyUpdateData[] monthlyUpdates;

    @JsonCreator
    public InputData(
            @JsonProperty("numberOfTurns") final
            int turns,
            @JsonProperty("initialData") final
            InitialData data,
            @JsonProperty("monthlyUpdates") final
            MonthlyUpdateData[] monthlyUpdates
    ) {
        this.turns = turns;
        this.data = data;
        this.monthlyUpdates = monthlyUpdates;
    }

    public MonthlyUpdateData[] getMonthlyUpdates() {
        return monthlyUpdates;
    }

    @Override
    public String toString() {
        return "InputData{" + "turns=" + turns + ", data=" + data + ", monthlyUpdates=" + Arrays
                .toString(monthlyUpdates) + '}';
    }

    public int getTurns() {
        return turns;
    }

    public InitialData getData() {
        return data;
    }
}
