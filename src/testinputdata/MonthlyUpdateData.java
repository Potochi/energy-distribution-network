package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Arrays;

@JsonRootName("monthlyUpdates")
public
final
class MonthlyUpdateData {
    private final ConsumerInputData[] newConsumers;
    private final CostChangeData[] costChanges;
    private final ProducerChangeData[] producerChangeData;

    @JsonCreator
    public MonthlyUpdateData(
            @JsonProperty("newConsumers") ConsumerInputData[] newConsumers,
            @JsonProperty("distributorChanges") CostChangeData[] costChanges,
            @JsonProperty("producerChanges") ProducerChangeData[] producerChanges) {
        this.newConsumers = newConsumers;
        this.costChanges = costChanges;
        this.producerChangeData = producerChanges;
    }

    public ConsumerInputData[] getNewConsumers() {
        return newConsumers;
    }

    public CostChangeData[] getCostChanges() {
        return costChanges;
    }

    public ProducerChangeData[] getProducerChangeData() {
        return producerChangeData;
    }

    @Override
    public String toString() {
        return "MonthlyUpdateData{"
                + "newConsumers=" + Arrays.toString(newConsumers)
                + ", costChanges=" + Arrays.toString(costChanges)
                + ", producerChangeData=" + Arrays.toString(producerChangeData)
                + '}';
    }
}
