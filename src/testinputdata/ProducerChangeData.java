package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final
class ProducerChangeData {
    private final int id;
    private final int energyPerDistributor;

    @JsonCreator
    public ProducerChangeData(
            @JsonProperty("id")
                    int id,
            @JsonProperty("energyPerDistributor")
                    int energyPerDistributor) {
        this.id = id;
        this.energyPerDistributor = energyPerDistributor;
    }

    public int getId() {
        return id;
    }

    public int getEnergyPerDistributor() {
        return energyPerDistributor;
    }

    @Override
    public String toString() {
        String sb = "ProducerChangeData{" + "id=" + id
                + ", energyPerDistributor=" + energyPerDistributor
                + '}';
        return sb;
    }
}
