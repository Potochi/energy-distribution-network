package testinputdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import entities.EnergyType;

public final
class ProducerInputData {
    private final int id;
    private final EnergyType energyType;
    private final int maxDistributors;
    private final double priceKW;
    private final int energyPerDistributor;

    @JsonCreator
    public ProducerInputData(
            @JsonProperty("id")
                    int id,
            @JsonProperty("energyType")
                    EnergyType energyType,
            @JsonProperty("maxDistributors")
                    int maxDistributors,
            @JsonProperty("priceKW")
                    double priceKW,
            @JsonProperty("energyPerDistributor")
                    int energyPerDistributor) {
        this.id = id;
        this.energyType = energyType;
        this.maxDistributors = maxDistributors;
        this.priceKW = priceKW;
        this.energyPerDistributor = energyPerDistributor;
    }

    public int getId() {
        return id;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public int getMaxDistributors() {
        return maxDistributors;
    }

    public double getPriceKW() {
        return priceKW;
    }

    @Override
    public String toString() {
        return "ProducerInputData{" + "id=" + id
                + ", energyType=" + energyType
                + ", maxDistributors=" + maxDistributors
                + ", priceKW=" + priceKW
                + ", energyPerDistributor=" + energyPerDistributor
                + '}';
    }

    public int getEnergyPerDistributor() {
        return energyPerDistributor;
    }
}
