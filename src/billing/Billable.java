package billing;

public
interface Billable {
    /**
     * Bills a billable entity
     *
     * @param amount of currency
     * @return transaction result
     */
    BillingResult bill(Integer amount);
}
