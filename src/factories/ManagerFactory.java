package factories;


import databases.Database;
import entities.Consumer;
import entities.Distributor;
import simulation.*;
import testinputdata.MonthlyUpdateData;

import java.util.List;

public final
class ManagerFactory {
    static final ManagerFactory INSTANCE = new ManagerFactory();

    private ManagerFactory() {

    }

    public static ManagerFactory getINSTANCE() {
        return INSTANCE;
    }

    /**
     * Creates a contract manager
     * <p>
     * The contract managers ensures that all active consumers have a contract from the
     * distributors
     *
     * @param consumers    consumer database
     * @param distributors distributor database
     * @return contract manager
     */
    public SimulationEntity getContractManager(final Database<Consumer> consumers,
                                               final Database<Distributor> distributors) {
        return new ConsumerContractManager(consumers, distributors);

    }

    /**
     * Creates a payment manager
     * <p>
     * The consumer payment manager ensures that all active consumers get
     * their income on each tick
     *
     * @param consumers consumer database
     * @return payment manager
     */
    public SimulationEntity getPaymentManager(final Database<Consumer> consumers) {
        return new ConsumerPaymentManager(consumers);

    }

    /**
     * Creates a distributor price updater
     * <p>
     * The price updater updates the price of each distributor on each tick
     *
     * @param distributors distributor database
     * @return price updater
     */
    public SimulationEntity getPriceUpdater(final Database<Distributor> distributors) {
        return new DistributorPriceUpdater(distributors);
    }

    /**
     * Creates a update manager
     * <p>
     * The update manager updates the databases on each tick with data stored
     * in updates. Each time the updates are applied, the internal index advances
     * to the next update
     *
     * @param consumers    consumer database
     * @param distributors distributor database
     * @param updates      update list
     * @return monthly update manager
     */
    public SimulationEntity getUpdateManager(
            final Database<Consumer> consumers,
            final Database<Distributor> distributors,
            final List<MonthlyUpdateData> updates
    ) {
        return new MonthlyUpdateManager(consumers, distributors, updates);
    }

}
