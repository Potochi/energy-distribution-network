package factories;


import databases.Database;
import entities.Producer;
import strategies.GreenStrategy;
import strategies.PriceStrategy;
import strategies.ProducerStrategy;
import strategies.QuantityStrategy;

public final
class StrategyFactory {

    private StrategyFactory() {

    }

    static ProducerStrategy getStrategy(String strategy, Database<Producer> producers) {
        return switch (strategy) {
            case "GREEN" -> new GreenStrategy(producers);
            case "PRICE" -> new PriceStrategy(producers);
            case "QUANTITY" -> new QuantityStrategy(producers);
            default -> null;
        };
    }

}
