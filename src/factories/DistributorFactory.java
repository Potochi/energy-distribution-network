package factories;

import databases.Database;
import entities.Distributor;
import entities.Producer;
import testinputdata.DistributorInputData;

public
final
class DistributorFactory
        implements Factory<Distributor, DistributorInputData> {
    private static final
    DistributorFactory INSTANCE = new DistributorFactory();

    private DistributorFactory() {

    }

    public static DistributorFactory getInstance() {
        return INSTANCE;
    }

    /**
     * Create distributor with strategy
     *
     * @param data      distributor data
     * @param producers producer data structure used for strategy
     * @return distributor
     */
    public Distributor create(final DistributorInputData data, Database<Producer> producers) {

        var strategy = StrategyFactory.getStrategy(data.getStrategy(), producers);

        return new Distributor(data.getContractLength(), data.getId(), data.getInfrastructureCost(),
                data.getProductionCost(), data.getInitialBudget(),
                strategy, data.getEnergyNeeded());
    }
}
