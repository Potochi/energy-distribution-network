package factories;

import entities.Consumer;
import testinputdata.ConsumerInputData;

public
final
class ConsumerFactory implements Factory<Consumer, ConsumerInputData> {
    private static final
    ConsumerFactory INSTANCE = new ConsumerFactory();

    private ConsumerFactory() {

    }

    public static ConsumerFactory getInstance() {
        return INSTANCE;
    }

    /**
     * Create consumer
     *
     * @param data consumer data
     * @return consumer
     */
    public Consumer create(final ConsumerInputData data) {
        return new Consumer(data.getId(), data.getInitialBudget(), data.getMonthlyIncome());
    }
}
