package entities;

import billing.Billable;
import billing.BillingResult;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import simulation.SimulationEntity;

@JsonIgnoreProperties({"active"})
public
final
class Consumer extends SimulationEntity implements Billable {


    @JsonProperty("id")
    private final Integer id;
    private final Integer income;
    @JsonProperty("isBankrupt")
    private boolean bankrupt;
    private boolean hasContract;
    @JsonProperty("budget")
    private Integer budget;

    public Consumer(final Integer id, final Integer budget, final Integer income) {
        this.id = id;
        this.budget = budget;
        this.income = income;
    }

    @Override
    public String toString() {
        return "Consumer{"
                + "id=" + id + ", contractSigned=" + hasContract + ", budget=" + budget
                + ", income=" + income + '}';
    }

    public boolean isBankrupt() {
        return bankrupt;
    }

    public boolean isHasContract() {
        return hasContract;
    }

    public void setHasContract(final boolean value) {
        hasContract = value;
    }

    @Override
    public BillingResult bill(final Integer amount) {
        if (amount > this.budget) {
            return BillingResult.FAILED;
        } else {
            this.budget -= amount;
            return BillingResult.SUCCESFUL;
        }
    }

    /**
     * Sets a consumer as bankrupt and deactivates the underlying simulation entity
     */
    public void bankrupt() {
        deactivate();
        this.bankrupt = true;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getBudget() {
        return this.budget;
    }

    @Override
    protected void tickInternal() {
        this.budget += income;
    }
}
