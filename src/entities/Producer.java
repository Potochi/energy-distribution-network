package entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public final
class Producer extends Observable {
    private final EnergyType energyType;
    private final Double price;
    private final Integer maxDistributor;
    private final Integer id;
    private final
    ArrayList<ArrayList<Integer>> distributorIdLog = new ArrayList<>();
    private ArrayList<Distributor> distributors = new ArrayList<>();
    private Integer distribution;

    public Producer(EnergyType energyType, double price, Integer maxDistributor, Integer id,
                    Integer distribution) {
        this.energyType = energyType;
        this.price = price;
        this.maxDistributor = maxDistributor;
        this.id = id;
        this.distribution = distribution;
    }

    public ArrayList<Distributor> getDistributors() {
        return distributors;
    }

    public void setDistributors(ArrayList<Distributor> distributors) {
        this.distributors = distributors;
    }

    public Integer getId() {
        return id;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public Double getPrice() {
        return price;
    }

    public Integer getMaxDistributor() {
        return maxDistributor;
    }

    public Integer getDistribution() {
        return distribution;
    }

    public boolean isFull() {
        return distributors.size() >= maxDistributor;
    }

    /**
     * Update producer max energy
     *
     * @param maxEnergy new max energy
     */


    public void update(Integer maxEnergy) {
        this.distribution = maxEnergy;
        this.setChanged();
        this.notifyObservers();
    }

    public double getCost() {
        return price * distribution;
    }

    /**
     * Get the current distributors that are given energy from this producer
     *
     * @return list containing id's
     */
    public List<Integer> getDistributorIds() {
        ArrayList<Integer> ids = new ArrayList<>();
        distributors.forEach(distributor -> ids.add(distributor.getId()));
        return ids;
    }

    /**
     * Return a list containing lists of distributor id's for each month the producer was active
     *
     * @return list of lists of id's
     */
    public ArrayList<ArrayList<Integer>> getDistributorIdLog() {
        return distributorIdLog;
    }

    /**
     * Log this months' distributors
     */
    public void tick() {
        distributorIdLog.add(new ArrayList<>(getDistributorIds()));
    }
}
