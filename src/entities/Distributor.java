package entities;

import billing.Billable;
import billing.BillingResult;
import databases.ContractDatabase;
import databases.Database;
import simulation.SimulationEntity;
import strategies.ProducerStrategy;
import testinputdata.CostChangeData;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public
final
class Distributor extends SimulationEntity implements Billable, Observer {
    private static final double PROFIT_RATE = 0.2;
    private static final double COST_RATIO = 10;

    private final Database<Contract> contracts = new ContractDatabase();
    private final Integer contractLength;
    private final Integer id;
    private final ArrayList<Contract> lateRemovalQueue = new ArrayList<>();
    private final ArrayList<Contract> immediateRemovalQueue = new ArrayList<>();
    private final ArrayList<Producer> producers = new ArrayList<>();
    private final Integer energyNeeded;

    private ProducerStrategy strategy;
    private Boolean checkProducers = true;
    private Integer contractCost = 0;
    private Integer infraCost;
    private Integer productionCost;
    private Integer budget;

    public Distributor(final Integer contractLength, final Integer id, final Integer infraCost,
                       final Integer productionCost, final Integer budget,
                       final ProducerStrategy strategy, Integer energyNeeded) {
        this.contractLength = contractLength;
        this.id = id;
        this.infraCost = infraCost;
        this.productionCost = productionCost;
        this.budget = budget;
        this.energyNeeded = energyNeeded;
        this.strategy = strategy;
    }

    public static double getProfitRate() {
        return PROFIT_RATE;
    }

    public ArrayList<Contract> getLateRemovalQueue() {
        return lateRemovalQueue;
    }

    public ArrayList<Producer> getProducers() {
        return producers;
    }

    public Integer getEnergyNeeded() {
        return energyNeeded;
    }

    public Integer getContractCost() {
        return contractCost;
    }

    public Integer getInfraCost() {
        return infraCost;
    }

    public Integer getProductionCost() {
        return productionCost;
    }

    public void setProductionCost(Integer productionCost) {
        this.productionCost = productionCost;
    }

    /**
     * Update the contract cost of the distributor using the current values for costs
     */
    public void
    updateContractCost() {
        if (contracts.getCollection().size() == 0) {
            this.contractCost = infraCost + productionCost + getProfit();
        } else {
            this.contractCost = Math.toIntExact(Math.round(
                    Math.floor((double) infraCost / contracts.getCollection().size())
                            + productionCost
                            + getProfit()));
        }
    }

    public Database<Contract> getContracts() {
        return contracts;
    }

    /**
     * Adds a contarct to the immediate removal queue
     * These contracts are removed after the distributor pays his taxes
     * in the same turn that they were added in the queue
     *
     * @param c
     */
    public void removeContractImmediate(final Contract c) {
        immediateRemovalQueue.add(c);
    }

    public Integer getDistributorPrice() {
        return contractCost;
    }

    /**
     * Updates the distributor with cost changes
     *
     * @param change cost changes
     */
    public void updateCosts(final CostChangeData change) {
        this.infraCost = change.getInfrastructureCost();
        setProductionCost(getCost());
    }

    private Integer getProfit() {
        return Math.toIntExact(Math.round(Math.floor(PROFIT_RATE * productionCost)));
    }

    /**
     * Updates the distributor contract price based on the amount
     * of contracts he currently has, and removes all of the contracts
     * that are in the lateRemovalQueue
     */
    public void updateAndRemove() {
        updateContractCost();
        contracts.getCollection().removeAll(lateRemovalQueue);
        lateRemovalQueue.clear();
    }

    @Override
    protected void tickInternal() {
        final int cost = infraCost + productionCost * contracts.getCollection().size();
        this.budget -= cost;
        if (budget < 0) {
            bankrupt();
            deactivate();
        }
        contracts.getCollection().removeAll(immediateRemovalQueue);
        immediateRemovalQueue.clear();
    }

    @Override
    public BillingResult bill(final Integer amount) {
        return null;
    }

    /**
     * Add funds to budget
     *
     * @param amount amount
     */
    public void addFunds(final Integer amount) {
        this.budget += amount;
    }

    /**
     * Adds a contract to the distributor
     *
     * @param c contract
     */
    public void addContract(final Contract c) {
        this.contracts.add(c);
    }

    /**
     * Bankrupts the distributor, deactivatin the underlying simulation entity and closing
     * all of it's contracts
     */
    public void bankrupt() {
        contracts.getStream().forEach(Contract::close);
        deactivate();
    }

    /**
     * Adds contract to the late removal queue
     * Contracts in this queue are removed the next turn
     * after the distributor updates the contract price
     *
     * @param c contract to be removed
     */
    public void lateRemoveContract(final Contract c) {
        lateRemovalQueue.add(c);
    }

    public Integer getContractLength() {
        return contractLength;
    }

    public Integer getId() {
        return id;
    }

    public Integer getBudget() {
        return budget;
    }

    public ArrayList<Contract> getImmediateRemovalQueue() {
        return immediateRemovalQueue;
    }

    @Override
    public void update(Observable o, Object arg) {
        this.checkProducers = true;
    }

    public Boolean getCheckProducers() {
        return checkProducers;
    }

    public void setCheckProducers(Boolean checkProducers) {
        this.checkProducers = checkProducers;
    }

    private int getCost() {
        double cost = producers.stream().mapToDouble(Producer::getCost).sum();
        return Math.toIntExact(Math.round(Math.floor(cost / COST_RATIO)));
    }

    public ProducerStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(ProducerStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Update producers
     * <p>
     * In case a producer changes it's cost, every distributor that currently
     * acquire energy from said producer will recalculate the most optimal producers
     * and acquire them
     */
    public void updateProducers() {
        Integer currentEnergy = 0;

        producers.forEach(producer -> {
            producer.deleteObserver(this);
            producer.getDistributors().remove(this);
        });
        producers.clear();

        var producerIterator = strategy.getIterator();
        while (currentEnergy < energyNeeded) {
            var producer = producerIterator.next();
            if (!producer.isFull()) {
                producer.getDistributors().add(this);
                producers.add(producer);
                producer.addObserver(this);
                currentEnergy += producer.getDistribution();
            }
        }

        setProductionCost(getCost());
        this.checkProducers = false;
    }
}
