package entities;

import billing.BillingResult;
import simulation.SimulationEntity;

public
final
class Contract extends SimulationEntity {

    private static final double PENALTY_RATE = 0.2;
    private final Distributor distributor;
    private final Consumer consumer;
    private final Integer rate;
    private Integer months;
    private Integer penalty = 0;

    public Contract(final Distributor distributor, final Consumer consumer) {
        this.distributor = distributor;
        this.consumer = consumer;
        this.rate = distributor.getDistributorPrice();
        this.months = distributor.getContractLength();
    }

    /**
     * Removes the contract from the consumer and adds it to the late removal
     * queue, used when the contract expires, or the distributor bankrupts
     */
    public void close() {
        consumer.setHasContract(false);
        distributor.lateRemoveContract(this);
        deactivate();
    }

    /**
     * Bankrupts the consumer and adds the contrac to the immediate removal
     * queue, used when the consumer breaches the contract
     */
    private void closeImmediate() {
        consumer.bankrupt();
        consumer.setHasContract(false);
        distributor.removeContractImmediate(this);
        deactivate();
    }

    @Override
    protected void tickInternal() {
        final var result = consumer.bill(rate + penalty);
        if (result == BillingResult.FAILED) {
            if (penalty != 0) {
                closeImmediate();
            } else {
                penalty = rate + Math.toIntExact(Math.round(PENALTY_RATE * rate));
            }
        } else {
            distributor.addFunds(rate + penalty);
            penalty = 0;
        }
        if (--months == 0) {
            if (penalty == 0) {
                close();
            }
        }
    }

    public Integer getConsumerId() {
        return consumer.getId();
    }


    public Integer getPrice() {
        return rate;
    }

    public Integer getRemaining() {
        return months;
    }
}
